$(document).ready(function() {
  localStorage.themes = '[\
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},\
  {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},\
  {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},\
  {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},\
  {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},\
  {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},\
  {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},\
  {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},\
  {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},\
  {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},\
  {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"} ]';

  if (!localStorage.selectedTheme) {
    localStorage.selectedTheme = '[{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}]';
  } else {
    changeColor(localStorage.selectedTheme);
  }
  var up = "https://png.icons8.com/collapse-arrow/win10/50/000000";
  var down = "https://png.icons8.com/expand-arrow/win10/50/000000";
  var hidden = false;
  $(".chat-text").bind("keyup", function(e) {
    if (e.keyCode == 13 ){
        var str = $("textarea").val();
        $("textarea").val('');
        $(".msg-insert").append("<div class=\"msg-send\">" + str + "</div>");
        $(".msg-insert").append("<div class=\"msg-receive\">" + str + "?</div>");
    }
  });

  $("img:last-child").bind("click", function () {
    $(".chat-body").toggle("slow");
    if(!hidden) {
        hidden = true;
        $("img").attr('src', up);
    } else {
        hidden = false;
        $("img").attr('src', down);
    }
  });

  $('.my-select').select2({
    'data': JSON.parse(localStorage.themes)
  });

  $('.apply-button').on('click', function(){
    var bgColor = $(".my-select").select2("data")[0]["bcgColor"];
    var fontColor = $(".my-select").select2("data")[0]["fontColor"];
    var temp = JSON.stringify([{"bcgColor" : bgColor, "fontColor" : fontColor}]);
    localStorage.selectedTheme = temp;
    changeColor(temp);
  });

  function changeColor(data) {
    var obj = JSON.parse(data)[0];
    $(".chat-head").css("background",obj["bcgColor"]);
    $("h2").css('color',obj['fontColor']);

    $(".calculator .model").css("background",obj["bcgColor"]);
    $("h1").css('color',obj['fontColor']);
  }

  $("#chatbox-input").keypress(function(e) {
    if (e.which === 13 && e.target.value !== '') {
      var inputVal = document.createElement("p");
      inputVal.innerHTML = $(this).val();
      inputVal.classList.add("msg-send");

      $(this).val('');
      $("#msg-body").append(inputVal);

      $(".chat-body").scrollTop($(".chat-body").prop("scrollHeight"));
      return false;
    }
  });
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
